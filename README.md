# How To AUTO Set Up an OpenVPN Server on Ubuntu 18.04 (The Better Way)

### Credit where credit is due. 
I give credit to the guys over at Digital Ocean for originally writing the tutorial for [Installing OpenVPN on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-18-04). If you don't know who Digital Ocean are then please check them out as they host guides/tutorials on how to set up your own self hosted software on local or remote machines and honestly their guides are second to none. 
### This Post. 
Although the guys over at Digital Ocean done a splendid job of writing a guide on Installing OpenVPN on Ubuntu 18.04, a guide which I have followed myself several times in fact. I thought as a first post this would be a good opportunity to improve on something that should be relatively simple but for a lot of people it usually ends up being quite difficult. To improve on this I have written a bash script to completely automate the Digital Ocean guide. Don't get me wrong, read their guides, try to learn something, better you understand why your writing long command's in terminal instead of blindly just copying and pasting anything you read online. For the people that already know what OpenVPN is and don't have time to learn something new, or the ones that just need it set up ASAP, this post is for you!
### The Script.
Feel free to use the script as and how you please, the code is posted below but if you would rather download it directly from my git repository then the link for that is below also. This script completely mimics the commands run on the Digital Ocean Guide except for one small factor. On the Digital Ocean guide you are required to have two separate servers and whilst setting up OpenVPN on one server your expected use the second server for signing and issuing certificates, although this is a very secure way of setting up OpenVPN and in some case scenario's I would recommend it, there's a lot of people out there that don't want to pay for two servers just to follow a guide on how to set up OpenVPN on one. The script below follows the exact guide but instead of using two servers all commands are run in two separate directories and the files are cleaned up and removed afterwards. This takes away the need for two servers and is still as secure. 
### Get the Script. 
```cd ~
git clone https://gitlab.com/Sutherland/openvpn-auto-install-18.04.git
```
### Run the Script.
```
cd ~/openvpn-auto-install-18.04
sudo bash ./auto-openvpn-18.04.sh
```
During installation you will be asked a total of 7 questions. the first 6 are whether or not you want to change the default locale values when generating the CA Certificates. The last question is how many client configurations you wan't to generate, and if your like me you'll need one for every device. 

If you read this and you would like to give any feedback or discuss the script any further, please email sutherland@scripting.online. 

Enjoy