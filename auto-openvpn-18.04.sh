#!/usr/bin/env bash

#INSTALLING PACKAGES
echo -e "######## \e[32mINSTALLING PACKAGES\e[39m ########"
echo -e ""

#INSTALLING UPDATES
sudo apt-get -qq update -y

#INSTALLING OPENVPN
sudo apt install -y openvpn wget curl
echo -e ""
echo -e "######## \e[32mGETTING EasyRSA\e[39m ########"
echo -e ""

#FETCHING EASYRSA
RSAURL=$(curl -fsSLI -o /dev/null -w %{url_effective} https://github.com/OpenVPN/easy-rsa/releases/latest)
RSAVERSION="${RSAURL##*/}"
RSAURL="$RSAURL/EasyRSA-unix-$RSAVERSION.tgz"
RSAURL="${RSAURL/tag/download}"
wget $RSAURL -O /tmp/EasyRSA.tgz
mkdir -p /tmp/EasyRSA && tar xf /tmp/EasyRSA.tgz -C /tmp/EasyRSA --strip-components=1
mkdir -p /tmp/EasyRSA-CA && tar xf /tmp/EasyRSA.tgz -C /tmp/EasyRSA-CA --strip-components=1
cp /tmp/EasyRSA-CA/vars.example /tmp/EasyRSA-CA/vars

#SETTING EASYRSA CONFIG SETTINGS
echo -e "######## \e[32mPROMPTING FOR CERTIFICATE DEFAULTS\e[39m ########"
echo -e ""
read -p "Enter Your Country [eg: US]: " country
read -p "Enter Your Povince [eg: California]: " province
read -p "Enter Your City [eg: San Francisco]: " city
read -p "Enter Your Organization [eg: Copyleft Certificate Co]: " organization
read -p "Enter Your EMAIL [eg: me@example.net]: " email
read -p "Enter Your Organizational Unit [eg: My Organizational Unit]: " organizationalunit

if [ -z "$country" ];
then
sed -i "s/#set_var\sEASYRSA_REQ_COUNTRY/set_var EASYRSA_REQ_COUNTRY/g" /tmp/EasyRSA-CA/vars
else
sed -i "s/#set_var\sEASYRSA_REQ_COUNTRY\s\"US\"/set_var EASYRSA_REQ_COUNTRY    \"$country\"/g" /tmp/EasyRSA-CA/vars
fi

if [ -z "$province" ];
then
sed -i "s/#set_var\sEASYRSA_REQ_PROVINCE/set_var EASYRSA_REQ_PROVINCE/g" /tmp/EasyRSA-CA/vars
else
sed -i "s/#set_var\sEASYRSA_REQ_PROVINCE\s\"California\"/set_var EASYRSA_REQ_PROVINCE   \"$province\"/g" /tmp/EasyRSA-CA/vars
fi

if [ -z "$city" ];
then
sed -i "s/#set_var\sEASYRSA_REQ_CITY/set_var EASYRSA_REQ_CITY/g" /tmp/EasyRSA-CA/vars
else
sed -i "s/#set_var\sEASYRSA_REQ_CITY\s\"San\sFrancisco\"/set_var EASYRSA_REQ_CITY       \"$city\"/g" /tmp/EasyRSA-CA/vars
fi

if [ -z "$organization" ];
then
sed -i "s/#set_var\sEASYRSA_REQ_ORG/set_var EASYRSA_REQ_ORG        /g" /tmp/EasyRSA-CA/vars
else
sed -i "s/#set_var\sEASYRSA_REQ_ORG\s\"Copyleft\sCertificate\sCo\"/set_var EASYRSA_REQ_ORG        \"$organization\"/g" /tmp/EasyRSA-CA/vars
fi

if [ -z "$email" ];
then
sed -i "s/#set_var\sEASYRSA_REQ_EMAIL/set_var EASYRSA_REQ_EMAIL/g" /tmp/EasyRSA-CA/vars
else
sed -i "s/#set_var\sEASYRSA_REQ_EMAIL\s\"me@example.net\"/set_var EASYRSA_REQ_EMAIL      \"$email\"/g" /tmp/EasyRSA-CA/vars
fi

if [ -z "$organizationalunit" ];
then
sed -i "s/#set_var\sEASYRSA_REQ_OU/set_var EASYRSA_REQ_OU/g" /tmp/EasyRSA-CA/vars
else
sed -i "s/#set_var\sEASYRSA_REQ_OU/set_var EASYRSA_REQ_OU/g" /tmp/EasyRSA-CA/vars
sed -i "s/\s\"My\sOrganizational\sUnit\"/       \"$organizationalunit\"/g" /tmp/EasyRSA-CA/vars
fi

echo -e ""

#INITIATING PKI
echo -e "######## \e[32mINITIATING PKI\e[39m ########"
(cd /tmp/EasyRSA-CA && ./easyrsa init-pki)
(cd /tmp/EasyRSA && ./easyrsa init-pki)

#BUILDING CA
echo -e "######## \e[32mBUILDING CA\e[39m ########"
(cd /tmp/EasyRSA-CA && echo -en "\n" | ./easyrsa build-ca nopass)

#CREATING PRIVATE KEY
echo -e "######## \e[32mCREATING PRIVATE KEY\e[39m ########"
(cd /tmp/EasyRSA && echo -en "\n" | ./easyrsa gen-req server nopass)
cp /tmp/EasyRSA/pki/private/server.key /etc/openvpn/

#IMPORTING PRIVATE KEY INTO CA
echo -e "######## \e[32mIMPORTING PRIVATE KEY\e[39m ########"
(cd /tmp/EasyRSA-CA && ./easyrsa import-req /tmp/EasyRSA/pki/reqs/server.req server)

#SIGNING PRIVATE KEY
echo -e "######## \e[32mSIGNING PRIVATE KEY\e[39m ########"
(cd /tmp/EasyRSA-CA && echo -en "yes\n" | ./easyrsa sign-req server server)

#BACKING UP KEYS TO OPENVPN
cp /tmp/EasyRSA-CA/pki/issued/server.crt /etc/openvpn/
cp /tmp/EasyRSA-CA/pki/ca.crt /etc/openvpn/

#GENERATING DH KEYS
echo -e "######## \e[32mGENERATING DH KEYS\e[39m ########"
(cd /tmp/EasyRSA && ./easyrsa gen-dh)

#GENERATING HMAC
(cd /tmp/EasyRSA && openvpn --genkey --secret ta.key)

#BACKING UP KEYS TO OPENVPN
cp /tmp/EasyRSA/ta.key /etc/openvpn/
cp /tmp/EasyRSA/pki/dh.pem /etc/openvpn/

#GENERATING CLIENT CONFIGS
echo -e "######## \e[32mGENERATING CLIENT CONFIGS\e[39m ########"
echo -e ""
mkdir -p ~/client-configs/keys
chmod -R 700 ~/client-configs
read -p "How many client configs do you want to generate [default: 1]: " configcount

configstart=1

if [ -z "$configcount" ];
then
configcount=1
fi

for (( c=$configstart; c<=$configcount; c++ ))
do
    (cd /tmp/EasyRSA && echo -en "\n" | ./easyrsa gen-req client$c nopass)
    cp /tmp/EasyRSA/pki/private/client$c.key ~/client-configs/keys/
    (cd /tmp/EasyRSA-CA && ./easyrsa import-req /tmp/EasyRSA/pki/reqs/client$c.req client$c)
    (cd /tmp/EasyRSA-CA && echo -en "yes\n" | ./easyrsa sign-req client client$c)
    cp /tmp/EasyRSA-CA/pki/issued/client$c.crt ~/client-configs/keys/
done

cp /tmp/EasyRSA/ta.key ~/client-configs/keys/
cp /etc/openvpn/ca.crt ~/client-configs/keys/

#CONFIGURING OPENVPN SERVICE
cp /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz /etc/openvpn/
(cd /etc/openvpn/ && gzip -d /etc/openvpn/server.conf.gz)

sed -i "s/tls-auth ta.key 0 # This file is secret/tls-auth ta.key 0 # This file is secret\nkey-direction 0/g" /etc/openvpn/server.conf
sed -i "s/cipher AES-256-CBC/cipher AES-256-CBC\nauth SHA256/g" /etc/openvpn/server.conf
sed -i "s/dh dh2048.pem/dh dh.pem/g" /etc/openvpn/server.conf
sed -i "s/;user nobody/user nobody/g" /etc/openvpn/server.conf
sed -i "s/;group nogroup/group nogroup/g" /etc/openvpn/server.conf
sed -i "s/;push \"redirect-gateway def1 bypass-dhcp\"/push \"redirect-gateway def1 bypass-dhcp\"/g" /etc/openvpn/server.conf
sed -i "s/;push \"dhcp-option DNS 208.67.222.222\"/push \"dhcp-option DNS 208.67.222.222\"/g" /etc/openvpn/server.conf
sed -i "s/;push \"dhcp-option DNS 208.67.220.220\"/push \"dhcp-option DNS 208.67.220.220\"/g" /etc/openvpn/server.conf
echo -e ""

#ADJUSTING NETWORK CONFIGURATION
echo -e "######## \e[32mADJUSTING NETWORK CONFIGURATION\e[39m ########"
echo -e ""

sed -i "s/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g" /etc/sysctl.conf
sudo sysctl -p

interface="$(ip route | grep default | sed 's/.*dev \(.*\) proto.*/\1/')"
sed -i "s/#   ufw-before-forward/#   ufw-before-forward\n#\n\n*nat\n:POSTROUTING ACCEPT [0:0]\n-A POSTROUTING -s 10\.8\.0\.0\/8 -o $interface -j MASQUERADE\nCOMMIT/g" /etc/ufw/before.rules
sed -i "s/DEFAULT_FORWARD_POLICY=\"DROP\"/DEFAULT_FORWARD_POLICY=\"ACCEPT\"/g" /etc/default/ufw

sudo ufw allow 1194/udp
sudo ufw allow OpenSSH
sudo ufw disable
sudo ufw --force enable

echo -e ""

#STARTING OPENVPN SERVICE
sudo systemctl start openvpn@server
sudo systemctl enable openvpn@server

#CREATING CLIENT CONFIG INFRASTRUCTURE
mkdir -p ~/client-configs/files
cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf ~/client-configs/base.conf

myip="$(dig +short myip.opendns.com @resolver1.opendns.com)"
sed -i "s/remote my-server-1 1194/remote $myip 1194/g" ~/client-configs/base.conf
sed -i "s/;user nobody/user nobody/g" ~/client-configs/base.conf
sed -i "s/;group nogroup/group nogroup/g" ~/client-configs/base.conf
sed -i "s/ca ca.crt/#ca ca.crt/g" ~/client-configs/base.conf
sed -i "s/cert client.crt/#cert client.crt/g" ~/client-configs/base.conf
sed -i "s/key client.key/#key client.key/g" ~/client-configs/base.conf
sed -i "s/tls-auth ta.key 1/#tls-auth ta.key 1/g" ~/client-configs/base.conf
sed -i "s/cipher AES-256-CBC/cipher AES-256-CBC\nauth SHA256\nkey-direction 1\n# script-security 2\n# up \/etc\/openvpn\/update-resolv-conf\n# down \/etc\/openvpn\/update-resolv-conf/g" ~/client-configs/base.conf

echo -e '#!/bin/bash' >> ~/client-configs/make_config.sh
echo -e "\n" >> ~/client-configs/make_config.sh
echo -e "# First argument: Client identifier" >> ~/client-configs/make_config.sh
echo -e "\n" >> ~/client-configs/make_config.sh
echo -e "KEY_DIR=~/client-configs/keys" >> ~/client-configs/make_config.sh
echo -e "OUTPUT_DIR=~/client-configs/files" >> ~/client-configs/make_config.sh
echo -e "BASE_CONFIG=~/client-configs/base.conf" >> ~/client-configs/make_config.sh
echo -e "\n" >> ~/client-configs/make_config.sh
echo -e "cat \${BASE_CONFIG} \\" >> ~/client-configs/make_config.sh
echo -e "    <(echo -e '<ca>') \\" >> ~/client-configs/make_config.sh
echo -e "    \${KEY_DIR}/ca.crt \\" >> ~/client-configs/make_config.sh
echo -e "    <(echo -e '</ca>\\\n<cert>') \\" >> ~/client-configs/make_config.sh
echo -e "    \${KEY_DIR}/\${1}.crt \\" >> ~/client-configs/make_config.sh
echo -e "    <(echo -e '</cert>\\\n<key>') \\" >> ~/client-configs/make_config.sh
echo -e "    \${KEY_DIR}/\${1}.key \\" >> ~/client-configs/make_config.sh
echo -e "    <(echo -e '</key>\\\n<tls-auth>') \\" >> ~/client-configs/make_config.sh
echo -e "    \${KEY_DIR}/ta.key \\" >> ~/client-configs/make_config.sh
echo -e "    <(echo -e '</tls-auth>') \\" >> ~/client-configs/make_config.sh
echo -e "    > \${OUTPUT_DIR}/\${1}.ovpn" >> ~/client-configs/make_config.sh

chmod 700 ~/client-configs/make_config.sh

#GENERATING OVPN FILES
for (( c=$configstart; c<=$configcount; c++ ))
do
    (cd ~/client-configs && sudo ./make_config.sh client$c)
done

#CLEANING UP TEMPORARY FILES
sudo rm -R /tmp/EasyRSA*

#INSTALLATION FINISHED
echo -e "######## \e[32mINSTALLATION FINISHED\e[39m ########"
echo -e ""
echo -e "Congratulations, installion is complete!"
echo -e ".ovpn connection files are available in ~/client-configs/files/"
